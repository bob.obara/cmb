cmake_minimum_required(VERSION 3.12)

# In order to use a new superbuild, please copy the item from the date stamped
# directory into the `keep` directory. This ensure that new versions will not
# be removed by the uploader script and preserve them for debugging in the
# future.

set(data_host "https://data.kitware.com")

# Determine the tarball to download.
# 20211111 – New SMTK post-processing API.
if ("$ENV{CMAKE_CONFIGURATION}" MATCHES "vs2019")
  set(file_item "618d7af02fa25629b9925eab")
  set(file_hash "7acc0f7192851b3162daad0080a0d8855ad7d83754ddef24886a4859b2b36fd8672af042b21beec2ff15d33f318256cb089ec3b1812eed727a3c02355d0bfdfb")
elseif ("$ENV{CMAKE_CONFIGURATION}" MATCHES "macos_arm64")
  set(file_item "618d7b382fa25629b9925ed6")
  set(file_hash "7fbc5498ee55e6e24721a485cc5e546c6334019de63dcca4f501c398be3638adb77c1a56a3a7b6b0c6484bb6465215a621684f80191e5845222777a1ea72eee9")
elseif ("$ENV{CMAKE_CONFIGURATION}" MATCHES "macos_x86_64")
  set(file_item "618d7b512fa25629b9925ef6")
  set(file_hash "07df29730afffd3a1d0742ac56724ef35ea5959af0c7399f2978e6a482d65435a2807d4c49b75b387691ad3d25c3c91c1cfccbfa8beebfd2276660c00e0262dc")
else ()
  message(FATAL_ERROR
    "Unknown build to use for the superbuild")
endif ()

# Ensure we have a hash to verify.
if (NOT DEFINED file_item OR NOT DEFINED file_hash)
  message(FATAL_ERROR
    "Unknown file and hash for the superbuild")
endif ()

# Download the file.
file(DOWNLOAD
  "${data_host}/api/v1/item/${file_item}/download"
  ".gitlab/superbuild.tar.gz"
  STATUS download_status
  EXPECTED_HASH "SHA512=${file_hash}")

# Check the download status.
list(GET download_status 0 res)
if (res)
  list(GET download_status 1 err)
  message(FATAL_ERROR
    "Failed to download superbuild.tar.gz: ${err}")
endif ()

# Extract the file.
execute_process(
  COMMAND
    "${CMAKE_COMMAND}"
    -E tar
    xf ".gitlab/superbuild.tar.gz"
  RESULT_VARIABLE res
  ERROR_VARIABLE err
  ERROR_STRIP_TRAILING_WHITESPACE)
if (res)
  message(FATAL_ERROR
    "Failed to extract superbuild.tar.gz: ${err}")
endif ()
