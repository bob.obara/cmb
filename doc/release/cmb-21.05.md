# CMB 21.05 Release Notes
This is the first release of SMTK that follows our new release naming convention where the major revision number  corresponds to the year of the release and the minor revision number corresponds to the month.

## Panel Changes
ModelBuilder now includes ParaView's information, statistics, light, and memory
inspection panels.

## Runtime Debugging
You can  set the `PV_DEBUG_LEAKS_VIEW` environment
variable when running ModelBuilder to debug memory leaks, although this feature is currently untested.

## Project Support
CMB ModelBuilder now supports SMTK's new Project Functionality. Previously,  Projects were limited to a use single attribute resource and one or two model resources. The new Project support is  can support any number of resources including custom ones,
with assigned roles. The new Project design can optionally be scoped to a list
of available operations and resource types. New Project instances store and
retrieve their contents as atomic operations with the file system.
Projects can now be described entirely using Python, making it
easier to implement custom project types for different solvers.

More information is available in the SMTK user guide and there is also a new
[create a project tutorial](https://smtk.readthedocs.io/en/latest/tutorials/create_a_project/index.html>).

## New About ModelBuilder Dialog
The About Dialog now displays information pertinent concerning CMB ModelBuilder including:

* Client Information
* Acknowledgments
* Licensing Information

## Improved Help Menu
The Help menu now provides access to the following online resources:

* CMB Website
* CMB Issue Tracker
* User Guides and Tutorials

## SMTK Related Changes

For additional information on changes to SMTK, please see [SMTK-21.05](https://gitlab.kitware.com/cmb/smtk/-/blob/master/doc/release/smtk-21.05.rst)
