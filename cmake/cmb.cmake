# A contract file for testing CMB within SMTK.

cmake_minimum_required(VERSION 3.8.2)
project(cmb)

include(ExternalProject)

ExternalProject_Add(cmb
  GIT_REPOSITORY "https://gitlab.kitware.com/cmb/cmb.git"
  GIT_TAG "origin/master"
  PREFIX cmb
  STAMP_DIR cmb/stamp
  SOURCE_DIR cmb/src
  BINARY_DIR cmb/build
  CMAKE_ARGS
    -DBUILD_SHARED_LIBS:BOOL=${BUILD_SHARED_LIBS}
    -Dcmb_enable_testing=ON
    -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
    -Dsmtk_DIR=${smtk_DIR}
    ${response_file}
  INSTALL_COMMAND ""
  TEST_COMMAND "${CMAKE_CTEST_COMMAND}" --output-on-failure
  TEST_BEFORE_INSTALL True
)

find_package(Git REQUIRED)

ExternalProject_Add_Step(cmb lfs-init
  COMMAND   "${GIT_EXECUTABLE}"
            lfs
            install
            --local
  DEPENDEES download
  DEPENDERS configure
  COMMENT   "Enabling LFS for cmb"
  WORKING_DIRECTORY <SOURCE_DIR>)

ExternalProject_Add_Step(cmb lfs-download
  COMMAND   "${GIT_EXECUTABLE}"
            lfs
            fetch
  DEPENDEES lfs-init
  DEPENDERS configure
  COMMENT   "Downloading LFS data for cmb"
  WORKING_DIRECTORY <SOURCE_DIR>)

ExternalProject_Add_Step(cmb lfs-checkout
  COMMAND   "${GIT_EXECUTABLE}"
            lfs
            checkout
  DEPENDEES lfs-download
  DEPENDERS configure
  COMMENT   "Checking out LFS data for cmb"
  WORKING_DIRECTORY <SOURCE_DIR>)
