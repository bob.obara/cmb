//=============================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=============================================================================
#include "mbLayoutSpec.h"

void mbLayoutSpec::layoutDockWidget(QMainWindow* mw, QDockWidget* dw)
{
  static QDockWidget* attDW = nullptr;
  static QDockWidget* resDW = nullptr;

  if (dw->objectName() == "pqSMTKAttributePanel")
  {
    dw->setVisible(true);
    attDW = dw;
  }
  else if (dw->objectName() == "pqSMTKResourcePanel")
  {
    dw->setVisible(true);
    resDW = dw;
  }
  else if (dw->objectName() == "outputWidgetDock")
  {
    dw->setVisible(true);
    mw->resizeDocks({ dw }, { 200 }, Qt::Vertical);
  }
  else
  {
    dw->setVisible(false);
  }

  if ((attDW != nullptr) && (resDW != nullptr))
  {
    mw->tabifyDockWidget(resDW, attDW);
    attDW = nullptr;
  }
}
